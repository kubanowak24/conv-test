__kernel void compute (__global const float* in, __global float* out){
	const uint index = get_global_id(0);
	out[index] = sin(in[index]/5) + cos(in[index/5]) + 1.0;
}