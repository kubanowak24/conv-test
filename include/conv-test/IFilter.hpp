#pragma once

class IFilter{
public:
	virtual std::vector<float> apply(
		const std::vector<float>& in,
		const std::vector<float>& filter,
		int image_width,
		int image_height) = 0;
};