#pragma once

#include <memory>

#include <opencv2/opencv.hpp>
#include <CL/cl2.hpp>
#include "IFilter.hpp"

class OpenClFilter: public IFilter{
private:
	// cl::Kernel kernel;
	cl::Context context;
	cl::Device device;
public:
	std::unique_ptr<cl::Program> program;
	std::unique_ptr<cl::Kernel> conv_kernel;
public:
	OpenClFilter(cl::Device device);
	virtual std::vector<float> apply(
		const std::vector<float>& in,
		const std::vector<float>& filter,
		int image_width,
		int image_height);
};