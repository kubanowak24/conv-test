#define HALF_FILTER 5

__kernel void conv (const __global float* in,
				__global float* out,
				__constant float* filter,
				__local float* cached){

	int i = get_global_id(0);
	int j = get_global_id(1);

	int il = get_local_id(0);
	int jl = get_local_id(1);
	int local_height = get_local_size(0);
	int local_width = get_local_size(1);

	int height = get_global_size(0);
	int width = get_global_size(1);

	int k_global = j + i*width;
	int k_local = jl + HALF_FILTER + (il + HALF_FILTER)*local_width;

	cached[k_local] = in[k_global];

	// pixel close to the corner of the image
	if( i < HALF_FILTER || i > height - HALF_FILTER ||
		j < HALF_FILTER || j > width - HALF_FILTER ){
			barrier(CLK_LOCAL_MEM_FENCE);
			return;
		}

	// pixel close to edge of the work group
	int close_left = 0, close_right = 0;
	if ( jl < HALF_FILTER ){
		close_left = 1;
		cached [ k_local - HALF_FILTER ] = in [ k_global - HALF_FILTER ];
	}
	else if( jl >= local_width - HALF_FILTER){
		close_right = 1;
		cached [ k_local + HALF_FILTER ] = in [ k_global + HALF_FILTER ];
	}

	int local_row_offset = HALF_FILTER * local_width;
	int global_row_offset = HALF_FILTER * width;
	int close_top = 0, close_bottom = 0;
	if ( il < HALF_FILTER ){
		close_top = 1;
		cached [ k_local - local_row_offset ] = in [ k_global - global_row_offset];
	}
	else if( il >= local_height - HALF_FILTER){
		close_bottom = 1;
		cached [ k_local + local_row_offset ] = in [ k_global + global_row_offset];
	}

	// pixel close to corner of the work group
	if(close_left && close_top ){
		cached [ k_local - local_row_offset - HALF_FILTER ] = in [ k_global - global_row_offset - HALF_FILTER ];
	}
	else if(close_left && close_bottom){
		cached [ k_local + local_row_offset - HALF_FILTER ] = in [ k_global + global_row_offset - HALF_FILTER ];
	}
	else if(close_right && close_top){
		cached [ k_local - local_row_offset + HALF_FILTER ] = in [ k_global - global_row_offset + HALF_FILTER ];
	}
	else if (close_right && close_bottom){
		cached [ k_local + local_row_offset + HALF_FILTER ] = in [ k_global + global_row_offset + HALF_FILTER ];
	}

	barrier(CLK_LOCAL_MEM_FENCE);

	float sum = 0.0;
	int fIndex = 0;
	for (int r = -HALF_FILTER; r <= HALF_FILTER; r++)
	{
		int in_row_offset = (i + r) * width;
		for (int c = -HALF_FILTER; c <= HALF_FILTER; c++, fIndex++)
		{
			sum += in[ (j + c) + in_row_offset ] * filter [ fIndex ];

			//sum += cached [ k_local + c + r*(local_width + HALF_FILTER)  ] * filter [fIndex];
		}
	}

	out[j + i * width] = sum;
	// out[ k_global ] = cached[ k_local ];
}