// build with: gcc main.c -o main -lOpenCL -lm

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <unistd.h>

#include <CL/cl.h>


void compute(const float* buf_in, float* buf_out, int n){
	for(int i=0; i<n; i++){
		buf_out[i] = sinf(buf_in[i]) + cosf(buf_in[i]);
	}
}


#define N 1000000
static float buf_in[N];
static float buf_out[N];

int main(){
	for (int i=0; i<N; i++){
		buf_in[i] = i;
	}

	clock_t begin = clock();
	compute(buf_in, buf_out, N);
	clock_t end = clock();
	printf("Regular took\t%f seconds\n", (double)(end-begin)/CLOCKS_PER_SEC);
	memset(buf_out, 0, sizeof(buf_out));

	size_t srcsize, worksize = N;

	cl_int error;
	cl_platform_id platform;
	cl_device_id device;
	cl_uint platforms, devices;

	error = clGetPlatformIDs(1, &platform, &platforms);
	error = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 1, &device, &devices);
	cl_context_properties properties[]={
		CL_CONTEXT_PLATFORM,
		(cl_context_properties) platform,
		0
	};

	cl_context context = clCreateContext(properties, 1, &device, NULL, NULL, &error);
	cl_command_queue cq = clCreateCommandQueue(context, device, 0, &error);

	char src[8196];
	FILE* file = fopen("compute.cl", "r");
	srcsize = fread(src, 1, sizeof(src), file);
	fclose(file);

	const char* srcptr[] = {src};
	cl_program prog = clCreateProgramWithSource(context,
		1, srcptr, &srcsize, &error);
	error = clBuildProgram(prog, 0, NULL, "", NULL, NULL);

	cl_mem mem1, mem2;
	mem1 = clCreateBuffer(context, CL_MEM_READ_ONLY, worksize*sizeof(float), NULL, &error);
	mem2 = clCreateBuffer(context, CL_MEM_WRITE_ONLY, worksize*sizeof(float), NULL, &error);
	

	cl_kernel k_comp = clCreateKernel(prog, "compute", &error);
	clSetKernelArg(k_comp, 0, sizeof(mem1), &mem1);
	clSetKernelArg(k_comp, 1, sizeof(mem2), &mem2);

	begin = clock();
	error = clEnqueueWriteBuffer(cq, mem1, CL_FALSE, 0, worksize*sizeof(float), buf_in, 0, NULL, NULL);
	error = clEnqueueNDRangeKernel(cq, k_comp, 1, NULL, &worksize, NULL, 0, NULL, NULL);
	error = clEnqueueReadBuffer(cq, mem2, CL_FALSE, 0, worksize*sizeof(float), buf_out, 0, NULL, NULL);
	error = clFinish(cq);
	end = clock();

	float sum = 0;
	for(int i=0; i<N; i++){
		sum += buf_out[i];
	}

	printf("OpenCL took\t%f seconds\n", (double)(end-begin)/CLOCKS_PER_SEC);

	return 0;
}