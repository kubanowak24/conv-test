#include <fstream>
#include <string>
#include <memory>
#include <type_traits>
#include <typeinfo>
#include <math.h>

#include <CL/cl2.hpp>

#include "conv-test/OpenClFilter.hpp"

OpenClFilter::OpenClFilter(cl::Device device):
		context({device}), device(device){
	cl::Program::Sources sources;

	std::ifstream ifs("../kernel/conv.cl");
	std::string kernel_code;
	std::ostringstream ss;
	ss << ifs.rdbuf();
	kernel_code = ss.str();

	sources.push_back({kernel_code.c_str(), kernel_code.length()});

	this->program  = std::unique_ptr<cl::Program>(new cl::Program(context, sources));

	if(this->program->build({this->device}) != CL_SUCCESS){
		std::cout<<"Error building: "<<this->program->getBuildInfo<CL_PROGRAM_BUILD_LOG>(this->device)<<std::endl;
	}

	// conv_kernel = std::unique_ptr<cl::Kernel>(new cl::Kernel(*program, "conv_kernel"));
}

std::vector<float> OpenClFilter::apply(const std::vector<float>& in,
										const std::vector<float>& filter,
										int image_width,
										int image_height){
	std::vector<float> out(in.size(), 0);

	cl::Buffer buffer_in(context, CL_MEM_READ_WRITE, sizeof(std::remove_reference<decltype(in)>::type::value_type)*in.size());
	cl::Buffer buffer_out(context, CL_MEM_READ_WRITE, sizeof(decltype(out)::value_type)*out.size());
	cl::Buffer buffer_filter(context, CL_MEM_READ_WRITE, sizeof(std::remove_reference<decltype(in)>::type::value_type)*filter.size());

	cl::Kernel conv(*program, "conv");
	conv.setArg(0, buffer_in);
	conv.setArg(1, buffer_out);
	conv.setArg(2, buffer_filter);
	int localMemSize = ( 16 + 2 * (sqrt(filter.size()) / 2) ) *
					( 16 + 2 * (sqrt(filter.size()) / 2) ); // 16x16 workgroup size, 11x11 filter size
	conv.setArg(3, sizeof(float)*localMemSize, nullptr);

	auto start = std::chrono::high_resolution_clock::now();
	cl::Event ew1, ew2, er, ek;
	cl_ulong time_start, time_end;

	cl::CommandQueue queue(context, device, CL_QUEUE_PROFILING_ENABLE);
	queue.enqueueWriteBuffer(buffer_in, CL_TRUE, 0, sizeof(std::remove_reference<decltype(in)>::type::value_type)*in.size(), in.data(), NULL, &ew1);
	queue.enqueueWriteBuffer(buffer_filter, CL_TRUE, 0, sizeof(std::remove_reference<decltype(filter)>::type::value_type)*filter.size(), filter.data(), NULL, &ew2);
	cl::vector<cl::Event> ev{ew1, ew2};
	queue.enqueueNDRangeKernel(conv, cl::NullRange, cl::NDRange(image_width, image_height), cl::NDRange(16, 16), &ev, &ek);
	queue.enqueueReadBuffer(buffer_out, CL_TRUE, 0, sizeof(decltype(out)::value_type)*out.size(), out.data(), NULL, &er);
	cl::finish();

	auto elapsed = std::chrono::high_resolution_clock::now() - start;
	long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
	std::cout<<"OpenCL:\t"<<microseconds<<std::endl;

	return out;
}