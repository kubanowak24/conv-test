#include <CL/cl2.hpp>
#include <iostream>
#include <opencv2/opencv.hpp>
#include <math.h>
#include <stdexcept>
#include <time.h>

#include "conv-test/OpenClFilter.hpp"

#define PI 3.1415

cl::Device openClGetDevice(cl_device_type device_type){
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);

	if(platforms.size() == 0){
		throw std::runtime_error("No devices found");
	}

	cl::Platform platform = platforms[0];
	// std::cout<<"Using platform: "<<platform.getInfo<CL_PLATFORM_NAME>()<<std::endl;

	std::vector<cl::Device> devices;
	platform.getDevices(device_type, &devices);
	if(devices.size() == 0){
		throw std::runtime_error("No devices found");
	}
	cl::Device device = devices[0];
	// std::cout<<"Using device: "<<device.getInfo<CL_DEVICE_NAME>()<<std::endl;
	
	return device;
}

std::vector<float> getGaussianFilter(int filter_size, float sigma){
	std::vector<float> filter;
	float sigma_sq = sigma * sigma;
	float exp_multiplier = 1.0/(2*PI*sigma_sq);
	float sum = 0.0;
	float v;
	int half_filter_size = (int)(filter_size/2);
	for(int i=-half_filter_size; i<=half_filter_size; i++){
		for(int j=-half_filter_size; j<=half_filter_size; j++){
			v = exp_multiplier * exp(-(j*j + i*i)/(2*sigma_sq));
			filter.push_back(v);
			sum += v;
		}
	}
	for(int i=0; i<filter_size; i++){
		for(int j=0; j<filter_size; j++){
			filter[i * filter_size + j] /= sum;
		}
	}
	return filter;
}

void filterImage(cv::Mat& mat_in, std::vector<float>& out, std::vector<float> filter){
	std::vector<float> in(mat_in.begin<float>(), mat_in.end<float>());
	int filter_size = sqrt(filter.size());
	int image_width = mat_in.cols;
	int half_filter_size = 2;
	for(int i=2; i<mat_in.rows-2; i++){
		for(int j=2; j<mat_in.cols-2; j++){
			float sum = 0.0;
			for (int r = -half_filter_size; r <= half_filter_size; r++)
			{
				int filter_row_offset = (r+half_filter_size) * filter_size;
				int in_row_offset = (i + r) * image_width;
				for (int c = -half_filter_size; c <= half_filter_size; c++)
				{
					sum += in[ (j + c) + in_row_offset ] * filter [ filter_row_offset + c + half_filter_size];
				}
			}
			out[j + i * image_width] = sum;
		}
	}
}

cv::Mat filterCvImage(cv::Mat& in, cv::Mat& filter){
	cv::Mat out(cv::Size(in.cols, in.rows), CV_32F);
	int filter_size = filter.rows;
	int image_width = in.cols;
	int half_filter_size = (int)(filter.rows/2);
	for(int i=half_filter_size; i<in.rows-half_filter_size; i++){
		for(int j=half_filter_size; j<in.cols-half_filter_size; j++){
			float sum = 0.0;
			for (int r = -half_filter_size; r <= half_filter_size; r++)
			{
				for (int c = -half_filter_size; c <= half_filter_size; c++)
				{
					sum += in.at<float>(i + r, j + c) * filter.at<float>(r+half_filter_size, c+half_filter_size);
				}
			}
			out.at<float>(i, j) = sum;
			if (i==5 && j==5){
				std::cout<<in.at<float>(i, j)<<std::endl;
			}
		}
	}
	return out;
}

int main(){
	cv::Mat img = cv::imread("../image/frame.tiff", CV_LOAD_IMAGE_GRAYSCALE);
	cv::Mat blurred;
	auto start = std::chrono::high_resolution_clock::now();
	cv::GaussianBlur(img, blurred, cv::Size(17, 17), 10.0);
	auto elapsed = std::chrono::high_resolution_clock::now() - start;
	long long microseconds = std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count();
	std::cout<<"OpenCV:\t"<<microseconds<<std::endl;
	cv::Mat resized;;

	cl::Device device = openClGetDevice(CL_DEVICE_TYPE_ALL);
	OpenClFilter f(device);

	cv::Mat input_img;
	img.convertTo(input_img, CV_32F);
	std::vector<float> in (input_img.begin<float>(), input_img.end<float>());
	int filter_size = 17;
	std::vector<float> filter = getGaussianFilter(filter_size, 10.0);

	std::vector<float> out = f.apply(in, filter, img.rows, img.cols);

	cv::Mat output_img(img.rows, img.cols, CV_32F, out.data());
	output_img.convertTo(output_img, CV_8U);
	cv::resize(output_img, resized, cv::Size(), 0.25, 0.25, CV_INTER_CUBIC);
	cv::imshow("OpenCl", resized);

	cv::waitKey(0);

	return 0;
}